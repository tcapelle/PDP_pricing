#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
from distutils.core import setup, Extension

# Nombre el módulo y archivos que contienen el código de fuente.
module = Extension("ptosLib", sources=["ptosLib.c"])

# Nombre del paquete, versión, descripción y una lista con las extensiones.
setup(name="Ptos Lib",
      version="1.0",
      description="Funciones raras para PG.",
      ext_modules=[module],
      include_dirs=[np.get_include()])
