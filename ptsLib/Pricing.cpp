#include "Pricing.h"

Pricing::Pricing() {
}

Pricing::~Pricing() {
}

PyObject* Pricing::pd(PyObject *Tmin, double *A, double *T, double *C, long *q, double *u, double *l, long N) {
    int source = 0;
    int target = N-1;
    int pMin = 1;
    int pMax = N/2;
    int dMin = pMax;
    int dMax = N-1;   
    vector<double> S0;
    vector<vector<double> *> h;
    int t = 0;
    clearCaminos();
    estados = 0;

    S0.push_back(0);
    S0.push_back(0);
    S0.push_back(0);
    S0.push_back(0);

    h.push_back(&S0);

    while(h.size() > 0) {
        vector<double> *label = h.back();
        h.pop_back();
        long v = label->at(label->size()-4);
        vector<int> *vecinos = findFirstNoneZeroInRowJ(A, v, N);

        //////////////////////////
        // POSIBLES EDGES
        //////////////////////////
        //V=vecinos
        //R=label[0:-3]
        //P=range(1,N/2)
        //D=range(N/2, N-1)
        //q=label[-1] 
        int terminal = 2*(pMax-1)+1;
        vector<double> pickup_posibles;
        vector<double> delivery_posibles;

        for(int p=pMin; p<pMax; p++) {
            if (!inVector(p, label, label->size()-3)) {
                pickup_posibles.push_back(p);              
            }
        }

        for(int d=dMin; d<dMax; d++) {
            if (!inVector(d, label, label->size()-3) && inVector(d-(pMax-1), label, label->size()-3)) {
                delivery_posibles.push_back(d);              
            }
        }

        if (label->at(label->size()-1) == 0) {
            delivery_posibles.push_back(terminal);
        }

        pickup_posibles.insert(pickup_posibles.end(), delivery_posibles.begin(), delivery_posibles.end()); //pickup_posibles = pickup_posibles + delivery_posibles

        vector<int> edges;
        for(long x=0; x<pickup_posibles.size(); x++) {
            for(long vx=0; vx<vecinos->size(); vx++) {
                if (pickup_posibles[x] == vecinos->at(vx)) {
                    edges.push_back(pickup_posibles[x]);
                }
            }
        }  
        //////////////////////////

        for(long x=0; x<edges.size(); x++) {
            PyObject *dict = PyDict_GetItem(Tmin, Py_BuildValue("i", edges[x])); 
            PyObject *val = PyDict_GetItem(dict, Py_BuildValue("i", edges[x]+pMax));
            if ((edges[x]>=pMin && edges[x]<pMax) && PyFloat_AsDouble(val)>u[edges[x]+pMax]) {
                printf("No hay nadie en este caso!\n");
                break;
            } else {
                int w = edges[x];
                double c_vw = getElemenetXY(C, w, v, N);
                double t_vw = getElemenetXY(T, w, v, N);
                long q_w = q[w];
                vector<double> *labelN = new vector<double>();
                bool extend = false;
                //////////////////////////
                // EXTENDS
                //////////////////////////
                double cost = label->at(label->size()-3) + c_vw;
                double t = label->at(label->size()-2) + t_vw;

                if (t <= u[w] && label->at(label->size()-1)+q_w<15) {
                    if (t <= l[w]) {
                        t = l[w];
                    }
                    for (int z=0; z<label->size()-3; z++) {
                        labelN->push_back(label->at(z));                        
                    }
                    labelN->push_back(w);
                    labelN->push_back(cost);
                    labelN->push_back(t);
                    labelN->push_back(label->at(label->size()-1)+q_w);
                    extend = true;
                } 
                //////////////////////////
                if (extend) {
                    h.push_back(labelN);
                    if (labelN->at(labelN->size()-4) == target) {
                        caminos.push_back(labelN);
                    }
                }
            }
            estados++;
        }      
    }    

    PyObject* cam = PyList_New(caminos.size());
    for(long i=0; i<caminos.size(); i++){
        PyObject* camsub = PyList_New(caminos[i]->size());
        for(long j=0; j<caminos[i]->size(); j++){
            PyList_SET_ITEM(camsub, j, Py_BuildValue("f", caminos[i]->at(j)));
        }
        PyList_SET_ITEM(cam, i, camsub);
    }
    return Py_BuildValue("(Oi)", cam, estados);
}



///////////////////////////
// PRIVATES
///////////////////////////
double Pricing::getElemenetXY(double *mat, long i, long j, long size) {
	return mat[j*size + i];
}

vector<int> *Pricing::findFirstNoneZeroInRowJ(double *mat, long j, long size) {
	long pos = 0;
    vector<int> *nonzeros = new vector<int>();
	while(pos < size) {
		if (getElemenetXY(mat, pos, j, size) != 0) {
			nonzeros->push_back(pos);
		}
		pos++;
	}
	return nonzeros;
}

void Pricing::clearCaminos() {
    for(int i=0; i<caminos.size(); i++) {
        caminos[i]->clear();
        delete caminos[i];
    }
    caminos.clear();
}

bool Pricing::inVector(long val, vector<double>* v, long limit) {
    long r=0;
    for(r=0; r<limit; r++) {
        if (val==v->at(r)) {
            return true;
        }
    }
    return false;
}
