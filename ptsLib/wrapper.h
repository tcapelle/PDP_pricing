#ifndef __WRAPPER_H
#define __WRAPPER_H
#include <Python.h>
#include "Pricing.h"


#ifdef __cplusplus
extern "C" {
#endif

    typedef struct Pricing Pricing;

    Pricing* newPricing();

    PyObject* PricingPD(Pricing* p, PyObject *Tmin, double *A, double *T, double *C, long *q, double *u, double *l, long N);

    void deletePricing(Pricing* p);

#ifdef __cplusplus
}
#endif
#endif
