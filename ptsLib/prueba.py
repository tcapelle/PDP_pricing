import networkx as nx
import numpy as np
import ptosLib

G = nx.read_gpickle("AA40_20_Depot0.gpickle")
#    G = nx.read_gpickle("test_graph")
Tmin =nx.all_pairs_dijkstra_path_length(G,weight = 't')
A = np.array(nx.to_numpy_matrix(G))
T = np.array(nx.to_numpy_matrix(G, weight='t'))
C = np.array(nx.to_numpy_matrix(G, weight='d'))
q = np.array([0]+[node[1]['demand'] for node in G.nodes(data=True) if node[1]['tipo'] != 'terminal']+[0])
u = np.array([node[1]['u'] for node in G.nodes(data=True)])
l = np.array([node[1]['l'] for node in G.nodes(data=True)])

a, b = ptosLib.pd(Tmin, A, T, C, q, u, l)
for l in a:
    print(l)
print(b)
