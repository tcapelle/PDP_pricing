#ifndef __PRICING_H
#define __PRICING_H
#include <stdio.h>
#include <vector>
#include <Python.h>
using namespace std;

class Pricing {
  private:
    vector<vector<double> *>caminos;
    int estados;

    double getElemenetXY(double *mat, long i, long j, long size);
    vector<int> *findFirstNoneZeroInRowJ(double *mat, long j, long size);   
    void clearCaminos();
    bool inVector(long val, vector<double>* v, long limit);

  public:
    Pricing();
    ~Pricing();
    PyObject* pd(PyObject *Tmin, double *A, double *T, double *C, long *q, double *u, double *l, long N);
};

#endif
