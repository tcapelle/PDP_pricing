#include "wrapper.h"

extern "C" {
        Pricing* newPricing() {
                return new Pricing();
        }

        PyObject* PricingPD(Pricing* p, PyObject *Tmin, double *A, double *T, double *C, long *q, double *u, double *l, long N) {
                return p->pd(Tmin, A, T, C, q, u, l, N);
        }

        void deletePricing(Pricing* p) {
                delete p;
        }
}
