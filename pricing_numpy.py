import networkx as nx
import time
import numpy as np
# from numba import jit
import networkx as nx
import time

Q = 20


def pd_numpy(A, T, C, u, l, q, Tmin):

    source = 0
    N = A.shape[0]
    target = N-1
    P = range(1,N/2)
    D = range(N/2,N-1)
    caminos = []

    S0 = [0 ,0, 0, 0] #[primeros l-2 elementos es la label, costo, tiempo]
    h = [S0] #lista de labels
    estados = 0
    t = 0
    # l = 1
    while h:

        label= h.pop()
        v = label[-4]  #nodo terminal de la label actual
        vecinos = A[v].nonzero()[0]
        edges = possible_edges(vecinos, label[0:-3], P, D, label[-1])

        for w in edges:
            if w in P and Tmin[w, w+N/2]>u[w+N/2]:
                print "no hay nadie en este caso!"
                break
            else:
                c_vw = C[v,w]
                t_vw = T[v,w]
                q_w = q[w]
                a, labelN = extend(label, w, c_vw, t_vw, q_w, u, l)
                if a==1:
                    h.append(labelN)
                    if labelN[-4]==target:
                        caminos.append(labelN)
            estados += 1

    return caminos, estados


def extend(label, w, c_vw, t_vw, q_w, u, l):
    
    cost = label[-3] + c_vw
    t = label[-2] + t_vw
    if (t <= u[w]) and label[-1]+q_w<15:
        if (t <= l[w]):    # lo puedo agregar!
            t = l[w]
        labelN = label[0:-3]
        labelN.append(w)
        labelN.append(cost)
        labelN.append(t)
        labelN.append(label[-1]+q_w)
        return 1, labelN
    else: 
        return 0, 0

def possible_edges(V, R, P, D, q):
    '''entrega los posibles vecinos con respecto a la ruta actual
    V: es el conjutno de vecinos G[v].keys()
    R: es la ruta actual
    P: son los pickups 
    D: los deliverys
        Para resumir, la funcion devuelve los pickups que no estan
        acualmente en el vehiculo, los deliverys que el correspondiente
        pickup esta en al vehiculo, intersectado con los nodos vecinos
        del nodo terminal de la ruta.
    q: la carga del vehiculo
    '''
    terminal = len(P)*2+1      

    pickup_posibles = []
    for x in P:
        if x not in R:
            pickup_posibles.append(x)

    delivery_posibles = []
    for x in D:
        if x not in R and x-len(P) in R:
            delivery_posibles.append(x)
    if q == 0:
        delivery_posibles.append(terminal)

    aux = pickup_posibles + delivery_posibles

    return [x for x in aux if x in V]

if __name__=='__main__':
    G = nx.read_gpickle("test_graph")
    Tmin = nx.all_pairs_dijkstra_path_length(G, weight = 't')
    Tmin_a = np.zeros((G.order(), G.order()))
    for x in range(82):
        for y in range(82):
            try: Tmin_a[x,y] = Tmin[x][y]
            except: pass

    A = np.array(nx.to_numpy_matrix(G))
    T = np.array(nx.to_numpy_matrix(G, weight='t'))
    C = np.array(nx.to_numpy_matrix(G, weight='d'))
    q = np.array([node[1]['demand'] for node in G.nodes(data=True)])
    u = np.array([node[1]['u'] for node in G.nodes(data=True)])
    l = np.array([node[1]['l'] for node in G.nodes(data=True)])

    # res = pd_numpy(A, T, C, u, l, q, Tmin_a)
