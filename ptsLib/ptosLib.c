#include <Python.h>
#include "numpy/arrayobject.h"
#include "wrapper.h"

static PyObject *pd(PyObject *self, PyObject *args) {
    PyArrayObject *A, *T, *C, *q, *u, *l;
    PyObject *Tmin, *ret;

    if (!PyArg_ParseTuple(args, "OOOOOOO", &Tmin, &A, &T, &C, &q, &u, &l))
        return NULL;

    int ndA = PyArray_NDIM(A);
    npy_intp* dimsA = PyArray_DIMS(A);
    double *dptrA = (double *)PyArray_DATA(A); 

    int ndT = PyArray_NDIM(T);
    npy_intp* dimsT = PyArray_DIMS(T);
    double *dptrT = (double *)PyArray_DATA(T); 

    int ndC = PyArray_NDIM(C);
    npy_intp* dimsC = PyArray_DIMS(C);
    double *dptrC = (double *)PyArray_DATA(C); 

    int ndq = PyArray_NDIM(q);
    npy_intp* dimsq = PyArray_DIMS(q);
    long *dptrq = (long *)PyArray_DATA(q); 

    int ndu = PyArray_NDIM(u);
    npy_intp* dimsu = PyArray_DIMS(u);
    double *dptru = (double *)PyArray_DATA(u); 

    int ndl = PyArray_NDIM(l);
    npy_intp* dimsl = PyArray_DIMS(l);
    double *dptrl = (double *)PyArray_DATA(l); 

    struct Pricing *p = newPricing();
    ret = PricingPD(p, Tmin, dptrA, dptrT, dptrC, dptrq, dptru, dptrl, *dimsl); /*** FALTA Tmin ***/

    Py_DECREF(Tmin);
    Py_DECREF(A);
    Py_DECREF(T);
    Py_DECREF(C);
    Py_DECREF(q);
    Py_DECREF(u);
    Py_DECREF(l);

    return ret;
}

static PyMethodDef libMethods[] = {
        {"pd", pd, METH_VARARGS,
         "Implementacion de la funcion pd(Tmin, A, T, C, q, u, l)"},
        {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC
initptosLib(void) {
    (void) Py_InitModule("ptosLib", libMethods);
    import_array();
}
